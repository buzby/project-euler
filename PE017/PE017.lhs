\documentclass[a4paper,12pt]{article}
%include polycode.fmt
%include myformat.fmt
\title{Problem 17}
\author{Edmond Buzby}
\date{Solved: June 6, 2020}

\renewcommand{\abstractname}{Number letter counts}
\setlength{\parindent}{0in}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[export]{adjustbox}

\begin{document}
\maketitle
\hrule

\begin{abstract}
\noindent
If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are $3 + 3 + 5 + 4 + 4 = 19$ letters used in total.\\
If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?\\
\\
\textbf{NOTE:} Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters. The use of ``and'' when writing out numbers is in compliance with British usage.\\
\hrule
\end{abstract}

This is quite an interesting problem to solve elegantly, as there are several nuances to the way English words for numbers are structured. However, as with many problems, recursion offers us a relatively elegant way of solving this problem.\\
\\
Our goal here is to be able to write a function that can return the letter count of the English word of a given number. We can start by hard-coding the solutions to what we will call some of the \emph{base} words for numbers in English, i.e. words that are frequently used in combination with other modifier words to create a number. In English, these are the numbers from 1 to 19, as well as the multiples of ten, from 20 to 90. All numbers in English can be represented as combinations of these with modifier words like `\emph{and}', `\emph{hundred}', `\emph{thousand}', etc.\\
\newpage
Next, we need to define the rules of how these all get combined to form an English word for a given number. For the sake of the exercise, we have defined rules up to (but not including) one million. These rules work recursively and are in addition to the hard-coded values:
\begin{enumerate}
    \item Any number that is divisible by 1,000 will have the result of the number divided by 1,000, plus 8 (eight letters in `\emph{thousand}').
    \item Numbers between 1,000 and 1,000,000 (not inclusive) will have the result of the remainder when divided by 1,000, plus the result of the value when this remainder is subtracted from the original number, plus 3 if the remainder is less than 100 in order to add `\emph{and}'.
    \item Any number that is divisible by 100 and less than 1,000 will have the result of the number divided by 100, plus 7 (seven letters in `\emph{hundred}').
    \item Numbers between 100 and 1,000 (not inclusive) will have the result of the remainder when divided by 100, plus the result of the value when this remainder is subtracted from the original number, plus 3 (three letters in `\emph{and}').
    \item Numbers between 20 and 100 (not inclusive), will have the result of the remainder when divided by 10, plus the result of the value when this remainder is subtracted from the original number.
\end{enumerate}

Let's write all of these into a function.
\begin{code}
letter_count :: Integral a => a -> a 
letter_count 1 = 3
letter_count 2 = 3
letter_count 3 = 5
letter_count 4 = 4
letter_count 5 = 4
letter_count 6 = 3
letter_count 7 = 5
letter_count 8 = 5
letter_count 9 = 4
letter_count 10 = 3
letter_count 11 = 6
letter_count 12 = 6
letter_count 13 = 8
letter_count 14 = 8
letter_count 15 = 7
letter_count 16 = 7
letter_count 17 = 9
letter_count 18 = 8
letter_count 19 = 8
letter_count 20 = 6
letter_count 30 = 6
letter_count 40 = 5
letter_count 50 = 5
letter_count 60 = 5
letter_count 70 = 7
letter_count 80 = 6
letter_count 90 = 6
letter_count n
    | mod n 1000 == 0 = letter_count {-"\Big"-}(div n 1000{-"\Big"-}) + 8
    | n > 1000 && n < 1000000 = letter_count hundreds + 
        letter_count (n - hundreds) + add_and
    | mod n 100 == 0 = letter_count {-"\Big"-}(div n 100{-"\Big"-}) + 7
    | n > 100 && n < 1000 = letter_count tens + 
        letter_count (n - tens) + 3
    | n > 20 && n < 100 = letter_count ones + letter_count (n - ones)
    | otherwise = 0
    where
        ones = mod n 10
        tens = mod n 100
        hundreds = mod n 1000
        add_and
            | hundreds < 100 = 3
            | otherwise = 0
\end{code}

Finally, our \texttt{main} function maps this function across all the numbers from 1 to 1,000 (inclusive), and then prints the sum of these results.

\begin{code}
main = print (sum (map letter_count [1..1000]))
\end{code}

\textbf{Solution:} 21124

\end{document}