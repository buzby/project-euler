\documentclass[a4paper,12pt]{article}
%include polycode.fmt
%include myformat.fmt

\title{Problem 1}
\author{Edmond Buzby}
\date{Solved: August 17, 2019}

\renewcommand{\abstractname}{Multiples of 3 and 5}
\setlength{\parindent}{0in}

\begin{document}
\maketitle
\hrule

\begin{abstract}
\noindent
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. \\
Find the sum of all the multiples of 3 or 5 below 1000. \\
\hrule
\end{abstract}

We create a function that takes any integer, generates a list of positive integers less than the given integer which are multiples of either 3 or 5, and then takes the sum of this list.

\begin{code}
num_sum :: Integral a => a -> a
num_sum n = sum {-"\Big"-}[x | x <- [1..n-1], mod x 3 == 0 || mod x 5 == 0{-"\Big"-}]
\end{code}

Our \texttt{main} function prints the output of providing our \texttt{num\_sum} function with 1000, which gives us our solution.

\begin{code}
main = print $ num_sum 1000
\end{code}

\textbf{Solution:} 233168

\end{document}