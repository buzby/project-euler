\documentclass[a4paper,12pt]{article}
%include polycode.fmt
%include myformat.fmt
%if False
\begin{code}
pow :: Integral a => a -> a -> a
pow a b = a ^ b
\end{code}
%endif

\title{Problem 6}
\author{Edmond Buzby}
\date{Solved: August 17, 2019}

\renewcommand{\abstractname}{Sum square difference}
\setlength{\parindent}{0in}

\begin{document}
\maketitle
\hrule

\begin{abstract}
\noindent
The sum of the squares of the first ten natural numbers is,
\[ 1^2 + 2^2 + ... + 10^2 = 385 \]
The square of the sum of the first ten natural numbers is,
\[ (1 + 2 + ... + 10)^2 = 55^2 = 3025 \]
Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is $3025 - 385 = 2640$. \\
Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum. \\
\hrule
\end{abstract}

To solve this problem we define a couple of functions to mirror the behaviour as set out by the problem. The first of these is to determine the sum of the squares of the first $n$ natural numbers.

\begin{code}
sum_square :: Integral a => a -> a
sum_square n = sum {-"\Big"-}(map (^2) [1..n]{-"\Big"-})
\end{code} 

Next we define a function that determines the square of the sum of the first $n$ natural numbers.

%{
%format sum a = "\Big(\displaystyle\sum" a "\Big)"
\begin{code}
square_sum :: Integral a => a -> a
square_sum n = pow (sum {-"\Big"-}[1..n{-"\Big"-}]) 2
\end{code}
%}

We then write a function that returns the difference of these two values for each given $n$.

\begin{code}
difference :: Integral a => a -> a
difference n = square_sum n - sum_square n
\end{code}

Our \texttt{main} function prints the value returned when 100 is provided as the input to \texttt{difference}.

\begin{code}
main = print $ difference 100
\end{code}

\textbf{Solution:} 25164150

\end{document}