\documentclass[a4paper,12pt]{article}
%include polycode.fmt
%include myformat.fmt
%if False
\begin{code}
pow :: Integral a => a -> a -> a
pow a b = a ^ b
\end{code}
%endif

\title{Problem 7}
\author{Edmond Buzby}
\date{Solved: August 17, 2019}

\renewcommand{\abstractname}{10001st prime}
\setlength{\parindent}{0in}

\begin{document}
\maketitle
\hrule

\begin{abstract}
\noindent
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13. \\
What is the 10001st prime number? \\
\hrule
\end{abstract}

As this is a simple problem, we only need to implement a na\"{i}ve prime verification, by checking for divisibility with all numbers less than the square root.

\begin{code}
is_prime :: Integral a => a -> Bool
is_prime n = and $ map (\x -> mod n x > 0) [2..floor (sqrt (fromIntegral n))]
\end{code}

Next we generate an infinite list of prime numbers as verified by the \texttt{is\_prime} function.

\begin{code}
primes :: Integral a => [a]
primes = [p | p <- [2..], is_prime p]
\end{code}

We then define a function that returns the $n$th prime from this list.

\begin{code}
nth_prime :: Integral a => Int -> a
nth_prime n = primes !! (n - 1)
\end{code}

Finally our \texttt{main} function returns the 10001st prime number as required by the problem.

\begin{code}
main = print $ nth_prime 10001
\end{code}

\textbf{Solution:} 104743

\end{document}