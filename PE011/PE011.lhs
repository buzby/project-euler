\documentclass[a4paper,12pt]{article}
%include polycode.fmt
%include myformat.fmt
\usepackage{color}

\title{Problem 11}
\author{Edmond Buzby}
\date{Solved: May 22, 2020}

\renewcommand{\abstractname}{Largest product in a grid}
\newcommand\redbf[1]{\textcolor{red}{\textbf{#1}}}
\setlength{\parindent}{0in}
\usepackage{amsmath}

\begin{document}
\maketitle
\hrule

\begin{abstract}
\noindent
In the $20\times20$ grid below, four numbers along a diagonal line have been marked in red.
\begin{center}\fontdimen1\font=12pt
08 02 22 97 38 15 00 40 00 75 04 05 07 78 52 12 50 77 91 08\\
49 49 99 40 17 81 18 57 60 87 17 40 98 43 69 48 04 56 62 00\\
81 49 31 73 55 79 14 29 93 71 40 67 53 88 30 03 49 13 36 65\\
52 70 95 23 04 60 11 42 69 24 68 56 01 32 56 71 37 02 36 91\\
22 31 16 71 51 67 63 89 41 92 36 54 22 40 40 28 66 33 13 80\\
24 47 32 60 99 03 45 02 44 75 33 53 78 36 84 20 35 17 12 50\\
32 98 81 28 64 23 67 10 \redbf{26} 38 40 67 59 54 70 66 18 38 64 70\\
67 26 20 68 02 62 12 20 95 \redbf{63} 94 39 63 08 40 91 66 49 94 21\\
24 55 58 05 66 73 99 26 97 17 \redbf{78} 78 96 83 14 88 34 89 63 72\\
21 36 23 09 75 00 76 44 20 45 35 \redbf{14} 00 61 33 97 34 31 33 95\\
78 17 53 28 22 75 31 67 15 94 03 80 04 62 16 14 09 53 56 92\\
16 39 05 42 96 35 31 47 55 58 88 24 00 17 54 24 36 29 85 57\\
86 56 00 48 35 71 89 07 05 44 44 37 44 60 21 58 51 54 17 58\\
19 80 81 68 05 94 47 69 28 73 92 13 86 52 17 77 04 89 55 40\\
04 52 08 83 97 35 99 16 07 97 57 32 16 26 26 79 33 27 98 66\\
88 36 68 87 57 62 20 72 03 46 33 67 46 55 12 32 63 93 53 69\\
04 42 16 73 38 25 39 11 24 94 72 18 08 46 29 32 40 62 76 36\\
20 69 36 41 72 30 23 88 34 62 99 69 82 67 59 85 74 04 36 16\\
20 73 35 29 78 31 90 01 74 31 49 71 48 86 81 16 23 57 05 54\\
01 70 54 71 83 51 54 69 16 92 33 48 61 43 52 01 89 19 67 48
\end{center}
The product of these numbers is $26 \times 63 \times 78 \times 14 = 1788696$.\\
What is the greatest product of four adjacent numbers in the same direction (up, down, left, right, or diagonally) in the $20\times20$ grid?\\
\hrule
\end{abstract}
\newpage
To solve this problem, we essentially run through every four adjacent numbers in each direction. There are four different directions that need to be checked - left/right, up/down, forward diagonal (i.e. left to right from top to bottom) and back diagonal (i.e. right to left from top to bottom).\\

First, we will need some functions from the \texttt{List} module.

\begin{code}
import Data.List
\end{code}

We also need to create a string to represent the grid.

%{
%format \ = "\;"
\begin{code}
grid :: [Char]
grid = "08 02 22 97 38 15 00 40 00 75 04 05 07 78 52 12 50 77 91 08 \
\49 49 99 40 17 81 18 57 60 87 17 40 98 43 69 48 04 56 62 00 \
\81 49 31 73 55 79 14 29 93 71 40 67 53 88 30 03 49 13 36 65 \
\52 70 95 23 04 60 11 42 69 24 68 56 01 32 56 71 37 02 36 91 \
\22 31 16 71 51 67 63 89 41 92 36 54 22 40 40 28 66 33 13 80 \
\24 47 32 60 99 03 45 02 44 75 33 53 78 36 84 20 35 17 12 50 \
\32 98 81 28 64 23 67 10 26 38 40 67 59 54 70 66 18 38 64 70 \
\67 26 20 68 02 62 12 20 95 63 94 39 63 08 40 91 66 49 94 21 \
\24 55 58 05 66 73 99 26 97 17 78 78 96 83 14 88 34 89 63 72 \
\21 36 23 09 75 00 76 44 20 45 35 14 00 61 33 97 34 31 33 95 \
\78 17 53 28 22 75 31 67 15 94 03 80 04 62 16 14 09 53 56 92 \
\16 39 05 42 96 35 31 47 55 58 88 24 00 17 54 24 36 29 85 57 \
\86 56 00 48 35 71 89 07 05 44 44 37 44 60 21 58 51 54 17 58 \
\19 80 81 68 05 94 47 69 28 73 92 13 86 52 17 77 04 89 55 40 \
\04 52 08 83 97 35 99 16 07 97 57 32 16 26 26 79 33 27 98 66 \
\88 36 68 87 57 62 20 72 03 46 33 67 46 55 12 32 63 93 53 69 \
\04 42 16 73 38 25 39 11 24 94 72 18 08 46 29 32 40 62 76 36 \
\20 69 36 41 72 30 23 88 34 62 99 69 82 67 59 85 74 04 36 16 \
\20 73 35 29 78 31 90 01 74 31 49 71 48 86 81 16 23 57 05 54 \
\01 70 54 71 83 51 54 69 16 92 33 48 61 43 52 01 89 19 67 48"
\end{code}
%}

We then read the grid string as a flat list of integers.

\begin{code}
numlst :: [Int]
numlst = map read (words grid)
\end{code}

Next we define four functions for each of the four directions described earlier. They each recursively work through a list of integers and determine the largest product of four adjacent numbers in their respective directions, each assuming the list represents a $20\times20$ grid.

\begin{code}
max_up_down :: [Int] -> Int
max_up_down [] = 0
max_up_down lst
    | length lst <= 60 = 0
    | otherwise = max (product (map (lst !!) [0,20..60])) $ 
        max_up_down $ tail lst
\end{code}
\begin{code}
max_left_right :: [Int] -> Int
max_left_right [] = 0
max_left_right lst
    | mod (length lst) 20 == 3 = max_left_right $ drop 3 lst
    | otherwise = max (product (map (lst !!) [0..3])) $ 
        max_left_right $ tail lst
\end{code}
\begin{code}
max_forward_diag :: [Int] -> Int
max_forward_diag [] = 0
max_forward_diag lst
    | length lst <= 63 = 0
    | mod (length lst) 20 == 3 = max_forward_diag $ drop 3 lst
    | otherwise = max (product (map (lst !!) [0,21..63])) $ 
        max_forward_diag $ tail lst
\end{code}
\begin{code}
max_back_diag :: [Int] -> Int
max_back_diag [] = 0
max_back_diag lst
    | length lst <= 57 = 0
    | mod (length lst) 20 == 0 = max_back_diag $ drop 3 lst
    | otherwise = max (product (map (lst !!) [0,19..57])) $ 
        max_back_diag $ tail lst
\end{code}

Finally, our \texttt{main} function maps each of these functions to the grid list, and prints the largest of these values.

\begin{code}
main = print $ maximum $ map (\f -> f numlst) 
    [max_up_down, max_left_right, max_forward_diag, max_back_diag]
\end{code}

\textbf{Solution:} 70600674

\end{document}