\documentclass[a4paper,12pt]{article}
%include polycode.fmt
%include myformat.fmt
\title{Problem 15}
\author{Edmond Buzby}
\date{Solved: June 2, 2020}

\renewcommand{\abstractname}{Lattice paths}
\setlength{\parindent}{0in}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[export]{adjustbox}

\begin{document}
\maketitle
\hrule

\begin{abstract}
\noindent
Starting in the top left corner of a 2×2 grid, and only being able to move to the right and down, there are exactly 6 routes to the bottom right corner.\\\\
\includegraphics[width=0.25\textwidth, center]{PE015}
\\
How many such routes are there through a 20×20 grid?\\
\hrule
\end{abstract}
\newpage
This problem is ultimately just a convoluted way of asking how many ways can you order a group of $r$ right movements and $d$ down movements, and can be solved trivially using combinatorics.\\
\\
Given an $r \times d$ grid, we know there must be a total of $r + d$ movements to get from the top left to the bottom right corner, and that any route must consist of exactly $r$ right movements, and $d$ down movements. Hence we are ultimately trying to find the number of unique combinations of $r$ right and $d$ down movements. This can be expressed as:

\begin{equation*}
\frac{(r + d)!}{r! \times d!}
\end{equation*}

Let's write this as a simple function.
%if False
\begin{code}
factorial :: Integral a => a -> a 
factorial n = product [1..n]
\end{code}
%endif
\begin{code}
grid_routes :: Integral a => a -> a -> a 
grid_routes r d = div (factorial (r + d)) (factorial r * factorial d)
\end{code}

Finally, our \texttt{main} function determines the number of grid routes of a $20 \times 20$ grid.

\begin{code}
main = print $ grid_routes 20 20
\end{code}

\textbf{Solution:} 137846528820

\end{document}