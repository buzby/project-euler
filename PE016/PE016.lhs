\documentclass[a4paper,12pt]{article}
%include polycode.fmt
%include myformat.fmt
\title{Problem 16}
\author{Edmond Buzby}
\date{Solved: June 4, 2020}

\renewcommand{\abstractname}{Power digit sum}
\setlength{\parindent}{0in}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[export]{adjustbox}

\begin{document}
\maketitle
\hrule

\begin{abstract}
\noindent
$2^{15} = 32768$ and the sum of its digits is $3 + 2 + 7 + 6 + 8 = 26$.\\
\\
What is the sum of the digits of the number $2^{1000}$?\\
\hrule
\end{abstract}

To solve this problem, we literally just need to write a function that takes a base number and an exponent, computes the result, converts the result to a string, converts each character into its corresponding integer, and then takes the sum of them all.\\
\\
First we need the \texttt{Data.Char} module for the helper function that converts characters of digits into integers.

\begin{code}
import Data.Char
\end{code}

Next we write the aforementioned function.

%if False
\begin{code}
pow b e = b^e
\end{code}
%endif

\begin{code}
power_digit_sum :: (Show a, Integral a) => a -> a -> a
power_digit_sum b e = sum {-"\Big"-}(map (fromIntegral . digitToInt) (show $ pow b e){-"\Big"-})
\end{code}

Finally we call our function with arguments 2 and 1000 in our \texttt{main} function and print the result.

\begin{code}
main = print $ power_digit_sum 2 1000
\end{code}

\textbf{Solution:} 1366

\end{document}