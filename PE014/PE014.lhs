\documentclass[a4paper,12pt]{article}
%include polycode.fmt
%include myformat.fmt
%if False
\begin{code}
pow :: Integral a => a -> a -> a
pow a b = a ^ b
\end{code}
%endif

\title{Problem 14}
\author{Edmond Buzby}
\date{Solved: June 1, 2020}

\renewcommand{\abstractname}{Longest Collatz sequence}
\setlength{\parindent}{0in}
\usepackage{amsmath}

\begin{document}
\maketitle
\hrule

\begin{abstract}
\noindent
The following iterative sequence is defined for the set of positive integers:
\begin{flalign*}
\indent
n &\to \frac{n}{2} \text{ ($n$ is even)}&&\\
n &\to 3n + 1 \text{ ($n$ is odd)}&&
\end{flalign*}
Using the rule above and starting with 13, we generate the following sequence:
\begin{align*}
13 \to 40 \to 20 \to 10 \to 5 \to 16 \to 8 \to 4 \to 2 \to 1
\end{align*}
It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.\\\\
Which starting number, under one million, produces the longest chain?\\\\
\textbf{NOTE:} Once the chain starts the terms are allowed to go above one million.\\
\hrule
\end{abstract}
\newpage
To solve this problem, first we will need a function that can compute the length of a Collatz sequence given a starting number. We can do this recursively, as the solution will always be one greater than the length of the Collatz sequence starting at the next iteration.
%{
%format * = " "
\begin{code}
collatz_length :: Integral a => a -> a
collatz_length 1 = 1
collatz_length n
    | even n = 1 + collatz_length {-"\Big"-}(div n 2{-"\Big"-})
    | otherwise = 1 + collatz_length (3*n + 1)
\end{code}
%}
Now we just need a function that determines the number that produces the longest chain given a maximum starting number. We do this by simply checking each number up to the maximum, and returning the number which generates the longest chain. This could probably be made more efficient by storing the values in each discovered sequence in case one of the numbers is greater than the starting number, as these numbers could be dismissed instantly, however, this naive function works fine here.

\begin{code}
longest_collatz :: Integral a => a -> a
longest_collatz 1 = 0
longest_collatz m = snd $ maximum [(c, n) | n <- [1..m-1], 
    let c = collatz_length n]
\end{code}

Finally, our \texttt{main} function returns the value less than one million that produces the longest Collatz sequence.

\begin{code}
main = print $ longest_collatz 1000000
\end{code}

\textbf{Solution:} 837799

\end{document}