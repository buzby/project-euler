\documentclass[a4paper,12pt]{article}
%include polycode.fmt
%include myformat.fmt

\title{Problem 5}
\author{Edmond Buzby}
\date{Solved: August 17, 2019}

\renewcommand{\abstractname}{Smallest multiple}
\setlength{\parindent}{0in}

\begin{document}
\maketitle
\hrule

\begin{abstract}
\noindent
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder. \\
What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20? \\
\hrule
\end{abstract}

We can solve this problem recursively, as we can determine the smallest positive number that is evenly divisible by all numbers from 1 to some number $m$ if we know the solution to $m-1$. Once we know the solution to $m-1$, we can determine the minimum number we need to multiply this by in order to be divisible by $m$. This can be computed by finding the remainder when dividing the previous solution by $m$, and then dividing $m$ by the greatest common divisor of $m$ and this remainder.

\begin{code}
min_even_dividend :: Integral a => a -> a
min_even_dividend 1 = 1
min_even_dividend m = prev * mult
    where
        prev = min_even_dividend (m - 1)
        remainder = mod prev m 
        mult = div m (gcd m remainder)
\end{code}

We can then simply run our \texttt{main} function by printing the value returned by giving \texttt{min\_even\_dividend} an input of 20.

\begin{code}
main = print $ min_even_dividend 20
\end{code}

\textbf{Solution:} 232792560

\end{document}