\documentclass[a4paper,12pt]{article}
%include polycode.fmt
%include myformat.fmt
%if False
\begin{code}
pow :: (Integral a, Show a) => a -> a -> a
pow a b = a ^ b
\end{code}
%endif

\title{Problem 67}
\author{Edmond Buzby}
\date{Solved: August 17, 2019}

\renewcommand{\abstractname}{Largest palindrome product}
\setlength{\parindent}{0in}

\begin{document}
\maketitle
\hrule

\begin{abstract}
\noindent
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is $9009 = 91 \times 99$. \\
Find the largest palindrome made from the product of two 3-digit numbers. \\
\hrule
\end{abstract}

First of all, we define a function that returns whether or not a number is palindromic by converting the number into a string and comparing that string to its reverse.

\begin{code}
is_palindrome :: (Integral a, Show a) => a -> Bool
is_palindrome n
    | numstr == reverse numstr = True
    | otherwise = False
    where
        numstr = show n
\end{code}

Our next function generates a list of palindromic numbers that are the product of two $n$-digit numbers, and returns the greatest of these values.

\begin{code}
max_palindrome :: (Integral a, Show a) => a -> a
max_palindrome n = maximum [a*b | a <- [(pow 10 (n-1))..(pow 10 n) - 1], 
    b <- [a..(pow 10 n) - 1], is_palindrome (a*b)]
\end{code}

Finally, we run our \texttt{main} function which uses the above function to print the greatest palindromic number that is the product of two 3-digit numbers.

\begin{code}
main = print $ max_palindrome 3
\end{code}

\textbf{Solution:} 906609

\end{document}