\documentclass[a4paper,12pt]{article}
%include polycode.fmt
%include myformat.fmt
\usepackage{color}

\title{Problem 67}
\author{Edmond Buzby}
\date{Solved: December 23, 2019}

\renewcommand{\abstractname}{Maximum path sum II}
\newcommand\redbf[1]{\textcolor{red}{\textbf{#1}}}
\setlength{\parindent}{0in}

\begin{document}
\maketitle
\hrule

\begin{abstract}
\noindent
By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.
\begin{center}\fontdimen2\font=12pt
\redbf{3}\\
\redbf{7} 4\\
2 \redbf{4} 6\\
8 5 \redbf{9} 3
\end{center}
That is, $3 + 7 + 4 + 9 = 23$. \\
Find the maximum total from top to bottom in\\ \texttt{triangle.txt}, a 15K text file containing a triangle with one-hundred rows. \\
\hrule
\end{abstract}

This is a more difficult version of Problem 18, as the triangle now has 100 rows, as opposed to 15. This shouldn't be a problem, as we solved the original problem with the foresight of having to solve this one, so we can mostly repeat our same solution here. \\

We essentially create a \emph{solution} triangle of the same size, and recursively solve the problem from the bottom row up, with each value in the solution triangle being the maximum of the two solutions beneath it, until we arrive at the final maximum solution at the top of the triangle. \\

We again use the $Array$ data structure within Haskell, which allows us to easily reconstruct and traverse the triangle in Haskell.

\begin{code}

import Data.Array

\end{code}
\pagebreak

Our first function takes a triangle as a list of integers and creates a two-dimensional array (matrix), with each row containing one more value than the previous (much the same as a triangle).

\begin{code}

triangle_arr :: (Integral e, Ix i, Integral i) => [e] -> Array (i, i) e
triangle_arr triangle = array {-"\Big"-}((0,0), (len, len){-"\Big"-}) 
                        {-"\Big"-}(zip {-"\Big"-}[(r, c) | r <- [0..len], c <- [0..r]{-"\Big"-}] triangle{-"\Big"-})
    where
        len = (floor (sqrt (fromIntegral ((length triangle)*2)))) - 1

\end{code}

Our second function is where the real magic happens. It takes the triangle array created by the previous function, and uses that to create the aforementioned solution triangle by populating the bottom row with the same values from the triangle, and each ascending value is the sum of the corresponding triangle value with the maximum of the two \emph{solutions} beneath it.

\begin{code}

max_path :: (Integral e, Ix i, Integral i) => Array (i, i) e -> e
max_path arr = max_path_arr!(0,0)
    where
        len = snd $ snd $ bounds arr
        max_path_arr = array {-"\Big"-}((0,0), (len, len){-"\Big"-}) 
                       {-"\Big"-}[{-"\Big"-}((r, c), f (r, c){-"\Big"-}) | r <- [0..len], c <- [0..r]{-"\Big"-}]
        f (r, c)
            | r == len = arr!(len, c)
            | otherwise = max {-"\Big"-}(max_path_arr!(r+1, c){-"\Big"-}) 
                          {-"\Big"-}(max_path_arr!(r+1, c+1){-"\Big"-}) + arr!(r, c)

\end{code}

Finally, we run our \texttt{main} function, which does two things. First, it reads the contents of the \texttt{triangle.txt} file into a list of integers, before passing to our functions to print the solution.

%format <- = "\leftarrow "

\begin{code}

main = do
    contents <- readFile "triangle.txt"
    print $ max_path $ triangle_arr $ map read (words contents)

\end{code}

\textbf{Solution:} 7273

\end{document}