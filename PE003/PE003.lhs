\documentclass[a4paper,12pt]{article}
%include polycode.fmt

\title{Problem 3}
\author{Edmond Buzby}
\date{Solved: August 17, 2019}

\renewcommand{\abstractname}{Largest prime factor}
\setlength{\parindent}{0in}

\begin{document}
\maketitle
\hrule

\begin{abstract}
\noindent
The prime factors of 13195 are 5, 7, 13 and 29. \\
What is the largest prime factor of the number 600851475143 ? \\
\hrule
\end{abstract}

This is quite straightforward when we import Haskell's prime number package. 

\begin{code}
import Math.NumberTheory.Primes
\end{code}

We simply write a function that takes any integer and returns the greatest prime factor returned by the \texttt{factorise} function.

\begin{code}
max_prime :: (UniqueFactorisation a, Integral a) => a -> a
max_prime = unPrime . maximum . map fst . factorise
\end{code}

Finally our \texttt{main} function prints the value returned by passing the problem value to the \texttt{max\_prime} function.

\begin{code}
main = do
    let num = 600851475143 :: Integer
    print $ max_prime num
\end{code}

\textbf{Solution:} 6857

\end{document}