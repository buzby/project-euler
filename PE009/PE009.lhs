\documentclass[a4paper,12pt]{article}
%include polycode.fmt
%include myformat.fmt
%if False
\begin{code}
pow :: Integral a => a -> a -> a
pow a b = a ^ b
\end{code}
%endif

\title{Problem 9}
\author{Edmond Buzby}
\date{Solved: August 17, 2019}

\renewcommand{\abstractname}{Special Pythagorean triplet}
\setlength{\parindent}{0in}
\usepackage{amsmath}

\begin{document}
\maketitle
\hrule

\begin{abstract}
\noindent
A Pythagorean triplet is a set of three natural numbers, $a < b < c$, for which,
\[ a^2 + b^2 = c^2 \]
For example, $3^2 + 4^2 = 9 + 16 = 25 = 5^2$.\\
There exists exactly one Pythagorean triplet for which $a + b + c = 1000$.\\
Find the product $abc$.\\
\hrule
\end{abstract}

To solve this problem, we use Euclid's formula for computing Pythagorean triples, which states that for any two arbitrary integers $m$ and $n$ such that $m > n > 0$, then $a = m^2 - n^2$, $b = 2mn$ and $c = m^2 + n^2$. Knowing this, we can generate a list of Pythagorean triples. Since we are looking for a triple such that $a + b + c = 1000$, then it stands that
\begin{align*}
m^2 - n^2 + 2mn + m^2 + n^2 &= 1000\\
2m^2 + 2mn &= 1000\\
m^2 + mn &= 500
\end{align*}

Since $m$ is necessarily greater than $n$, we can determine the maximum value of $m$ by assuming $n = 1$.
\begin{align*}
m^2 + m &= 500\\
m &= \frac{-1 + \sqrt{1^2 - 4 \times 1 \times -500}}{2 \times 1}\\
&= \frac{\sqrt{2001} - 1}{2}\\
&< 22
\end{align*}

Therefore, the largest value that $m$ can be is 21. Knowing this, we can generate a reasonably finite list which we know will contain a triple that sums to \emph{exactly} 1000.

%format * = " "
\begin{code}
abc1000 :: Integral a => (a, a, a)
abc1000 = head [((pow m 2) - (pow n 2), 2*m*n, (pow m 2) + (pow n 2)) |
    m <- [2..21], n <- [1..m], m*(m + n) == 500]
\end{code}

Once we have obtained our Pythagorean triple, we run our \texttt{main} function which evaluates and prints the product of this triple.

\begin{code}
main = print $ (\(a, b, c) -> a*b*c) abc1000
\end{code}

\textbf{Solution:} 31875000

\end{document}