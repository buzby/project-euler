\documentclass[a4paper,12pt]{article}
%include polycode.fmt
%include myformat.fmt
%if False
\begin{code}
pow :: Integral a => a -> a -> a
pow a b = a ^ b
\end{code}
%endif

\title{Problem 10}
\author{Edmond Buzby}
\date{Solved: August 17, 2019}

\renewcommand{\abstractname}{Summation of primes}
\setlength{\parindent}{0in}
\usepackage{amsmath}

\begin{document}
\maketitle
\hrule

\begin{abstract}
\noindent
The sum of the primes below 10 is $2 + 3 + 5 + 7 = 17$.\\
Find the sum of all the primes below two million.\\
\hrule
\end{abstract}

Again, as this is a fairly simple problem, we only need a na\"{i}ve prime verification function that simply checks for divisibility against all numbers less than the square root.

\begin{code}
is_prime :: Integral a => a -> Bool
is_prime n = and $ map (\x -> mod n x > 0) [2..floor (sqrt (fromIntegral n))]
\end{code}

Next we just write a function that takes an upper limit $n$ and takes the sum of the list of all prime numbers less than $n$.

\begin{code}
sum_primes :: Integral a => a -> a 
sum_primes n = sum {-"\Big"-}[p | p <- [2..n], is_prime p{-"\Big"-}]
\end{code}

Finally we run our \texttt{main} function which evaluates and prints the solution with an upper limit of two million.

\begin{code}
main = print $ sum_primes 2000000
\end{code}

\textbf{Solution:} 142913828922

\end{document}